pipeline {
    agent any 
    
    tools {nodejs "NodeJs"}
    environment {
        PROJECT_NAME = "Simple-Node-App"
        NODE_BUILD_URL = "http://3.109.160.162:3006"
    }

    triggers {
        pollSCM('*/20 * * * *')
    }

    stages {
        stage ('Send Notification') {
           steps {
            script {
                def branch = env.GIT_BRANCH
                def commit = sh(returnStdout: true, script: 'git log -1 --pretty=%B').trim()
                def author = sh(returnStdout: true, script: 'git log -1 --pretty=%an').trim()
                def url = env.BUILD_URL
                
                blocks = [
                    [
                        "type": "header",
                        "text": [
                            "type": "plain_text",
                            "text": "Build has been initiated for ${PROJECT_NAME}: ${branch}"
                        ]
                    ],
                    [
                    "type": "divider"
                    ],
                    [
                    "type": "section",
                    "text": [
                        "type": "mrkdwn",
                        "text": "<${url}|Build URL>"
                    ]
                    ],
                    [
                        "type": "section",
                        "fields": [
                            [
                            "type": "mrkdwn",
                            "text": "*Author:* \n ${author}"
                            ],
                            [
                            "type": "mrkdwn",
                            "text": "*Branch:* \n ${branch}"
                            ],
                            [
                            "type": "mrkdwn",
                            "text": "*Commit message:* \n ${commit}"
                            ]
                        ]
                    ]
                ]
                
                // Send the message to Slack
                slackSend(channel: '#slack-integration-test', color: 'warning', blocks: blocks)
            }
            }
        }
        stage('Build') {
            steps {
                sh 'git checkout master'
                sh 'git pull origin master'
                sh 'exit 1'
                sh 'npm install' // install project dependencies
                sh 'npm run build' // build project
            }
        }
        stage('Copy to AWS') {
            steps {
                withCredentials([[
                    $class: 'AmazonWebServicesCredentialsBinding',
                    credentialsId: "aws-jenkins",
                    accessKeyVariable: 'AWS_ACCESS_KEY_ID',
                    secretKeyVariable: 'AWS_SECRET_ACCESS_KEY'
                ]]) {
                   sh 'scp -o StrictHostKeyChecking=no -r build ubuntu@3.109.160.162:/home/ubuntu/test_scp/'
                   echo "pipeline successfully works!!!"
                }
            }
        }
    }

     post {
        success {
            script {
                def branch = env.GIT_BRANCH
                def commit = sh(returnStdout: true, script: 'git log -1 --pretty=%B').trim()
                def author = sh(returnStdout: true, script: 'git log -1 --pretty=%an').trim()
                def url = env.BUILD_URL
                
                blocks = [
                    [
                        "type": "header",
                        "text": [
                            "type": "plain_text",
                            "text": "Build Successful for ${PROJECT_NAME}: ${branch} :tada: :clinking_glasses: :raised_hands:"
                        ]
                    ],
                    [
                      "type": "divider"
                    ],
                    [
                      "type": "section",
                      "text": [
                          "type": "mrkdwn",
                          "text": "*URL:*\n${NODE_BUILD_URL}"
                      ]
                    ],
                    [
                        "type": "section",
                        "fields": [
                            [
                            "type": "mrkdwn",
                            "text": "*Author:* \n ${author}"
                            ],
                            [
                            "type": "mrkdwn",
                            "text": "*Branch:* \n ${branch}"
                            ],
                            [
                            "type": "mrkdwn",
                            "text": "*Commit message:* \n ${commit}"
                            ]
                        ]
                    ]
                ]
                
                // Send the message to Slack
                slackSend(channel: '#slack-integration-test', color: 'good', blocks: blocks)
            }
        }
        failure {
            script {
                def branch = env.GIT_BRANCH
                def commit = sh(returnStdout: true, script: 'git log -1 --pretty=%B').trim()
                def author = sh(returnStdout: true, script: 'git log -1 --pretty=%an').trim()
                def url = env.BUILD_URL
                
                blocks = [
                    [
                        "type": "header",
                        "text": [
                            "type": "plain_text",
                            "text": "Build Failed for ${PROJECT_NAME}: ${branch} :face_with_rolling_eyes: :expressionless:"
                        ]
                    ],
                    [
                      "type": "divider"
                    ],
                    [
                      "type": "section",
                      "text": [
                          "type": "mrkdwn",
                          "text": "<${url}|Failed URL>"
                      ]
                    ],
                    [
                        "type": "section",
                        "fields": [
                            [
                            "type": "mrkdwn",
                            "text": "*Author:* \n ${author}"
                            ],
                            [
                            "type": "mrkdwn",
                            "text": "*Branch:* \n ${branch}"
                            ],
                            [
                            "type": "mrkdwn",
                            "text": "*Commit message:* \n ${commit}"
                            ]
                        ]
                    ]
                ]
                
                // Send the message to Slack
                slackSend(channel: '#slack-integration-test', color: 'danger', blocks: blocks)
            }
        }
    }
}

